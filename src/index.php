<?php

// Pandoc convert
$action = filter_input(INPUT_GET, 'action', FILTER_DEFAULT);

switch ($action) {
    case 'pandoc-supported':
        $command = 'command -v /usr/local/bin/pandoc >/dev/null';
        list($ret_val, $output) = execute_shellscript(
            $command
        );
        break;
    case 'convert':
        $from = htmlspecialchars($_GET['from']);
        $to = htmlspecialchars($_GET['to']);
        $data = [];
        $data = file_get_contents('php://input');
        $temp_file = tempnam(sys_get_temp_dir(), 'pandoc.tmp');
        if (list($dom, $errors) = is_html($data)) {
            $first_title = first_title($dom);
            $images = get_images($dom);
            $data = track_iframes($dom, $data);
        } else {
            $data = htmlspecialchars($data);
        }
        file_put_contents($temp_file, $data);
        $command = sprintf(
            'pandoc -f %s -t %s %s',
            $from,
            $to,
            $temp_file
        );
        list($ret_val, $output) = execute_shellscript(
            $command
        );
        unlink($temp_file);
        break;
    case 'list-input-formats':
        list($ret_val, $output) = execute_shellscript(
            'pandoc --list-input-formats'
        );
        break;
    case 'list-output-formats':
        list($ret_val, $output) = execute_shellscript(
            'pandoc --list-output-formats'
        );
        break;
    default:
        header("HTTP/1.0 404 Not Found");
        die('Please provide data to the service.');
}

function first_title($dom)
{
    if (get_class($dom) !== 'DOMDocument') {
        return '';
    }

    $xpath = new \DOMXPath($dom);
    foreach ($xpath->query("//h1") as $item) {
        // return $dom->saveHtml($item);
        return $item->textContent;
    }

    return '';
}

function get_images($dom)
{
    $images = [];

    $xpath = new \DOMXPath($dom);
    foreach ($xpath->query("//img") as $item) {
        if ($item->hasAttribute('src')) {
            $src = $item->getAttribute('src');
            $name = strtolower($src);
            $name = basename($name);
            $name = str_replace(' ', '-', $name);
            $images[] = (object) [
                'name' => $name,
                'src' => $src,
                'title' => $item->getAttribute('title'),
                'alt' => $item->getAttribute('alt')
            ];
        }
    }

    return $images;
}

function track_iframes($dom, $data)
{
    $xpath = new \DOMXPath($dom);
    foreach ($xpath->query("//iframe") as $item) {
        if ($item->hasAttribute('src')) {
            // replace iframe in data with the data found on the iframe remote, such as coding examples
            $src = $item->getAttribute('src');
            if (strpos($src, '//') == 0) {
                // Relative to https
                $src = 'https:' . $src;
            } elseif (strpos($src, '/') == 0) {
                // Wont be able to  do anything about this relative path
                return $data;
            }
            try {
                // $iframe_content = file_get_contents($src);
                // $data = str_replace(search, $iframe_content, $data);
                // echo '<pre>' . print_r($iframe_content) . '</pre>';
            } catch (Exception $ex) {
                // printf('<iframe src="%s"></iframe>', $src);
            }
        }
    }

    return $data;
}

function is_html($content)
{
    try {
        if (strpos($content, '<body') === false) {
            $content = '<body>' . $content . '</body>';
        }
        if (strpos($content, 'DOCTYPE') === false) {
            $content = '<!DOCTYPE html>' .
                '<html>' .
                '<head>' .
                '<meta charset="utf-8">' .
                '</head>' .
                $content .
                '</html>';
        }

        $dom = new \DOMDocument('1.0');

        libxml_use_internal_errors(true);

        // load the html into the object
        $dom->loadHTML($content);

        $errors = libxml_get_errors();

        return [
            $dom,
            $errors
        ];
    } catch (\Exception $ex) {
        return [
            '',
            [
                $ex->getMessage()
            ]
        ];
    }
}

function execute_shellscript($command)
{
    $output = '';
    $ret_val = -1;
    exec($command, $output, $ret_val);
    return [
        $ret_val,
        $output
    ];
}

$result = [
    'ret_val' => $ret_val,
    'success' => $ret_val == 0,
    'result' => $output
];

if (isset($errors) && !empty($errors)) {
    $result['errors'] = $errors;
}

if (isset($first_title) && !empty($first_title)) {
    $result['first_title'] = $first_title;
}

if (isset($images)) {
    $result['images'] = $images;
}

header('Content-Type: application/json');
echo json_encode($result);
